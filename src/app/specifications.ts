export const SpecificationModel = {
  label: '',
  dataTab: 'model-sheet-1',
  characteristics: [
    {
      header: 'Основні',
      values: [
        {
          label: 'Загальна потужність',
          value: ''
        },
        {
          label: 'Розгін 0-100 км / год',
          value: ''
        },
        {
          label: 'Середній запас ходу електро – EVDB',
          value: ''
        },
        {
          label: 'Розмір АКБ: повний / корисний',
          value: ''
        },
        {
          label: 'Бортове зарядний пристрій – АС',
          value: ''
        },
        {
          label: 'Бортове зарядний пристрій – DC',
          value: ''
        },
        {
          label: 'Час заряду від розетки (220В) – AC',
          value: ''
        },
        {
          label: 'Час заряду від прискореної зарядки (380) – AC',
          value: ''
        },
        {
          label: 'Час заряду від швидкої зарядки до 80% – DC',
          value: ''
        },
        {
          label: 'Привід',
          value: ''
        },
        {
          label: 'Клас',
          value: ''
        }
      ]
    },
    {
      header: 'Кузов',
      values: [
        {
          label: 'Тип кузова',
          value: ''
        },
        {
          label: 'Кількість дверей',
          value: ''
        },
        {
          label: 'Кількість місць',
          value: ''
        },
        {
          label: 'Довжина',
          value: ''
        },
        {
          label: 'Ширина',
          value: ''
        },
        {
          label: 'Висота',
          value: ''
        },
        {
          label: 'Колісна база',
          value: ''
        },
        {
          label: 'Кліренс',
          value: ''
        },
        {
          label: 'Споряджена маса',
          value: ''
        },
        {
          label: 'Повна маса',
          value: ''
        },
        {
          label: 'Обсяги багажника',
          value: ''
        },
        {
          label: 'Максимальний обсяг багажника',
          value: ''
        }
      ]
    },
    {
      header: 'Мотор і трансмісія',
      values: [
        {
          label: 'Загальна потужність',
          value: ''
        },
        {
          label: 'Потужність ДВЗ',
          value: ''
        },
        {
          label: 'Потужність електромотора',
          value: ''
        },
        {
          label: 'Крутящий момент - загальний',
          value: ''
        },
        {
          label: 'Крутящий момент ДВЗ',
          value: ''
        },
        {
          label: 'Крутящий момент електромотора',
          value: ''
        },
        {
          label: 'Двигун',
          value: ''
        },
        {
          label: 'Кількість циліндрів',
          value: ''
        },
        {
          label: 'Кількість передач',
          value: ''
        }
      ]
    },
    {
      header: 'Інша інформація',
      values: [
        {
          label: 'Модель',
          value: ''
        },
        {
          label: 'Максимальна швидкість',
          value: ''
        },
        {
          label: 'Максимальна швидкість електро',
          value: ''
        },
        {
          label: 'Розгін 0-100 км/год',
          value: ''
        },
        {
          label: 'Електричний запас ходу',
          value: ''
        },
        {
          label: 'Витрата палива 25/50/100 км',
          value: ''
        },
        {
          label: 'Витрата при розрядженій батареї',
          value: ''
        },
        {
          label: 'Обсяг бака',
          value: ''
        },
        {
          label: 'Коефіцієнт опору',
          value: ''
        },
        {
          label: 'Викиди вуглецю (NEDC)',
          value: ''
        },
        {
          label: 'Розмір коліс',
          value: ''
        },
        {
          label: 'Тип коннектора зарядки',
          value: ''
        }
      ]
    }
  ]
};

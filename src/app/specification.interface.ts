export interface Specification {
  label: string;
  dataTab: string;
  characteristics: Characerictic[];
}

export interface Characerictic {
  header: string;
  values: CharacteristicItem[];
}

export interface CharacteristicItem {
  label: string;
  value: string;
}

import { Component, OnInit, ElementRef, ViewChild, ViewEncapsulation, NgZone } from '@angular/core';
import { Specification } from './specification.interface';
import { SpecificationModel } from './specifications';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  @ViewChild('specificationsHtml') public specificationsHtml: ElementRef;
  @ViewChild('pricesHtml') public pricesHtml: ElementRef;

  jsonInput: string;
  specificationsData: any;
  pricesData: any;
  testSpecificationsJson: any;
  testPricesJson: string;
  isCodeShown = false;
  specificationsModel: Specification[] = [];

  constructor(
    private _ngZone: NgZone
  ) {

  }

  ngOnInit() {
    this.testSpecificationsJson = require('../assets/specifications.json');
    this.testPricesJson = require('../assets/prices.json');
    this.specificationsModel.push(JSON.parse(JSON.stringify(SpecificationModel)));
  }

  parseSpecificationsData() {
    this.pricesData = null;
    this.isCodeShown = false;
    this.specificationsData = JSON.parse(this.jsonInput);
  }

  parsePricesData() {
    this.specificationsData = null;
    this.isCodeShown = false;
    this.pricesData = JSON.parse(this.jsonInput);
  }

  insertTestSpecificationsJson() {
    this.jsonInput = JSON.stringify(this.testSpecificationsJson);
    this.pricesData = null;
    this.isCodeShown = false;
  }

  insertTestPricesJson() {
    this.jsonInput = JSON.stringify(this.testPricesJson);
    this.specificationsData = null;
    this.isCodeShown = false;
  }

  addSpecification() {
    const newModel = Object.assign({}, JSON.parse(JSON.stringify(SpecificationModel)), this.specificationsModel[0]);
    newModel.dataTab = `model-sheet-${this.specificationsModel.length + 1}`;
    this.specificationsModel.push(newModel);
  }

  createHtml() {
    this.isCodeShown = true;
    const htmlValue = this.specificationsData ? this.specificationsHtml.nativeElement.outerHTML : this.pricesHtml.nativeElement.outerHTML;
    const printContainer1: HTMLElement = document.querySelector('#print-container1');
    const printContainer2: HTMLElement = document.querySelector('#print-container2');
    printContainer1.innerText = htmlValue;
    printContainer2.innerText = htmlValue;
  }

  copyText() {
    const copyBox = document.createElement('textarea');
    const value = this.specificationsData ? this.specificationsHtml.nativeElement.outerHTML : this.pricesHtml.nativeElement.outerHTML;
    copyBox.style.position = 'fixed';
    copyBox.style.left = '0';
    copyBox.style.top = '0';
    copyBox.style.opacity = '0';
    copyBox.value = value;
    document.body.appendChild(copyBox);
    copyBox.focus();
    copyBox.select();
    document.execCommand('copy');
    document.body.removeChild(copyBox);
  }

  showFormSpecificationPreview() {
    this.specificationsData = this.testSpecificationsJson;
    this.specificationsData.specifications = this.specificationsModel;
    console.log(this.specificationsData);
  }
}

